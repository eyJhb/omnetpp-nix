let
  nixpkgs = import (builtins.fetchTarball "https://github.com/nixos/nixpkgs/archive/2a35f664394b379e0c0785cc769ff6ccc791be39.tar.gz") {};
  # geos version 3.5.0
  nixpkgsGeos = import (builtins.fetchTarball "https://github.com/nixos/nixpkgs/archive/dc5b2741c984750f856c55824204b3e4ccc89292.tar.gz") {};
in { pkgs ? nixpkgs }:

let
  defaultScope = with pkgs; {
    inherit (qt5) qtbase wrapQtAppsHook;
    inherit (xorg) libX11 libXrender libXtst;
    jdk = jdk11;
    gtk = gtk3;
  } ;

  nixPkgs =  rec {
    callPackage = pkgs.newScope (defaultScope // nixPkgs);
    omnetpp = callPackage ./. { };
    # keetchi = callPackage ./keetchi.nix { };
    inet = callPackage ./inet.nix { };
    batman-ex = callPackage ./batman-ex.nix { inet = inet; };
    # ops = callPackage ./ops.nix { };
    osgearth = callPackage ./osgearth.nix { gdal = pkgs.gdal_2; geos = nixpkgsGeos.geos; };
  };

in nixPkgs
