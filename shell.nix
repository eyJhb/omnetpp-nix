with import <nixpkgs> {};

let
  opkgs = import ./pkgs.nix {};

  inetPatched = opkgs.inet.overrideAttrs (old: old // { patches = [ ./run_inet.patch ]; } );

in mkShell {
  shellHook = ''
    # setup our env correctly
    PATH=${opkgs.omnetpp}/bin:$PATH
    # PATH=${opkgs.inet}/src:$PATH
    PATH=${inetPatched}/src:$PATH
  '';
}
